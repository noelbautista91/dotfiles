#!/usr/bin/env bash

# Check if program is already running.
function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

# Insert run scripts here. 
run redshift -O 3000 && nordvpn connect

