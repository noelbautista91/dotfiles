# If you come from bash you might have to change your $PATH.
export LD_LIBRARY_PATH=/usr/lib
export GOPATH=$HOME/goprojects
export GOBIN=$GOPATH/bin
export PATH=$HOME/bin:/usr/local/bin:/usr/local/go/bin:$GOPATH:$GOBIN:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh
eval "$(rbenv init -)"

# Engage vi mode, press esc to normal mode
bindkey -v

# Preferred editor for local and remote sessions
export EDITOR='vim'
export SUDO_EDITOR='vim'

# Use Clang
export CC=clang
export CXX=clang++

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# Aliases and functions
alias pbcopy="xsel --clipboard --input"
alias pbpaste="xsel --clipboard --output"
alias del="rm -rfv"
alias sauce="source ./env/bin/activate"
alias v="vim"
alias n="newsboat"
alias c="cashtrack"
alias cl="cashtrack list"

