;;;; Welcome to Noy's Emacs Config
;;;; All packages must be manually installed instead of using MELPA, etc.
;;;; This is to ensure compatibility and reducing needless frequent updates.
;;;; Packages should be installed in directory ~/.emacs.d/<package-name>
;;;; Don't forget to leave comments explaining what each config does.

;;;; Vim Emulation
;;; Enable Vi Keybindings
(add-to-list 'load-path "~/.emacs.d/evil")
(require 'evil)
(evil-mode 1)

;;; :q kill the current buffer
(evil-ex-define-cmd "q" 'kill-buffer)

;;; :Q kill the current buffer and window
(evil-ex-define-cmd "Q" 'kill-buffer-and-window)

;;; :quit close emacs entirely
(evil-ex-define-cmd "quit" 'evil-quit-all)

;;;; Which Key (Show hints)
;;; Enable
(add-to-list 'load-path "~/.emacs.d/which-key")
(require 'which-key)
(which-key-mode)

;;;; Window Settings
;;; Display dired on side
(add-to-list 'load-path "~/.emacs.d/dash.el")
(add-to-list 'load-path "~/.emacs.d/dired-sidebar")
(add-to-list 'load-path "~/.emacs.d/dired-hacks")
(require 'dired-sidebar)
(evil-ex-define-cmd "d" 'dired)

;;; Show Line Numbers
(global-display-line-numbers-mode)

;;; Hide Menubar
(menu-bar-mode -1)

;;; Hide Toolbar
(tool-bar-mode -1)

;;; Use y or n instead of yes or no
(defalias 'yes-or-no-p 'y-or-n-p)

;;; Projectile for searching inside projects
(add-to-list 'load-path "~/.emacs.d/projectile")
(require 'projectile)
(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;;; Navigate windows
(eval-after-load "evil"
  '(progn
     ;; C-h focus on window to left of current window
     (define-key evil-normal-state-map (kbd "C-h") 'evil-window-left)
     ;; C-j focus on window below of current window
     (define-key evil-normal-state-map (kbd "C-j") 'evil-window-down)
     ;; C-k focus on window above current window
     (define-key evil-normal-state-map (kbd "C-k") 'evil-window-up)
     ;; C-l focus on window to right of current window
     (define-key evil-normal-state-map (kbd "C-l") 'evil-window-right)))

;;; Activate Purpose
(add-to-list 'load-path "~/.emacs.d/emacs-purpose")
(add-to-list 'load-path "~/.emacs.d/imenu-list")
(require 'imenu-list)
(require 'window-purpose)
(require 'window-purpose-x)
(purpose-mode)
(purpose-x-kill-setup)

;;; Adoc Layout
(add-to-list 'purpose-user-mode-purposes '(adoc-mode . adoc))
(purpose-compile-user-configuration)

;;;; File Manager (Dired)
;;; :o open dired in current directory instead of asking for confirmation
(require 'dired-x)
(defun open-dired-other-window ()
  (interactive)
  (let ((buf (dired-sidebar-toggle-sidebar)))
    (switch-to-buffer (other-buffer buf))
    (switch-to-buffer-other-window buf)))
(evil-ex-define-cmd "o" 'open-dired-other-window)

;;;; EShell
;;; :sh invoke EShell
(defun open-eshell-other-window ()
  (interactive)
  (let ((buf (eshell)))
    (switch-to-buffer (other-buffer buf))
    (switch-to-buffer-other-window buf)))
(evil-ex-define-cmd "sh" 'open-eshell-other-window)

;;;; Magit
(add-to-list 'load-path "~/.emacs.d/magit/lisp")
(add-to-list 'load-path "~/.emacs.d/with-editor")
(add-to-list 'load-path "~/.emacs.d/magit-popup")
(add-to-list 'load-path "~/.emacs.d/graphql")
(add-to-list 'load-path "~/.emacs.d/treepy")
(add-to-list 'load-path "~/.emacs.d/ghub")
(add-to-list 'load-path "~/.emacs.d/evil-magit")
(require 'with-editor)
(require 'graphql)
(require 'ghub)
(require 'treepy)
(require 'magit-popup)
(require 'magit)
(require 'evil-magit)

(with-eval-after-load 'info
  (info-initialize)
  (add-to-list 'Info-directory-list
	       "~/.emacs.d/magit/Documentation/"))

;;;; Programming Modes
;;; Company (Auto-complete)
(add-to-list 'load-path "~/.emacs.d/company")
(add-to-list 'load-path "~/.emacs.d/company-go")
(require 'company)
(require 'company-tng)
(add-hook 'after-init-hook 'global-company-mode)

;;; Golang
(require 'company-go)
(autoload 'go-mode "go-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))
(add-hook 'go-mode-hook
      (lambda ()
        (set (make-local-variable 'company-backends) '(company-go))
        (company-mode)))

;;; Use tab key to cycle through suggestions.
;;; ('tng' means 'tab and go')
(company-tng-configure-default)

;;; Wrap around next and prev items in list
(setq company-selection-wrap-around t)

;;; Show suggestions after entering one character.
(setq company-minimum-prefix-length 1)

;;; No delay in showing suggestions.
(setq company-idle-delay 0)
`
;;; Adoc
(add-to-list 'load-path "~/.emacs.d/adoc-mode")
(add-to-list 'load-path "~/.emacs.d/markup-faces")
(add-to-list 'auto-mode-alist (cons "\\.adoc\\'" 'adoc-mode))
(add-to-list 'auto-mode-alist (cons "\\.asciidoc\\'" 'adoc-mode))
(autoload 'adoc-mode "adoc-mode" "Adoc editing mode." t)

;;; Kotlin
(add-to-list 'load-path "~/.emacs.d/kotlin-mode")
(autoload 'kotlin-mode "kotlin-mode" "Kotlin editing mode." t)
(add-to-list 'auto-mode-alist '("\\.kt\\'" . kotlin-mode))

;;; Lua
(add-to-list 'load-path "~/.emacs.d/lua-mode")
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
(add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

;;; Go
(add-to-list 'load-path "~/.emacs.d/go-mode")
(autoload 'go-mode "go-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))
(when (string-equal system-type "gnu/linux")
  (setenv "PATH" (concat (getenv "PATH") ":/usr/local/go/bin:~/goprojects/bin/gocode"))
  (setq exec-path (append exec-path '(
				      "/usr/local/go/bin"
				      "~/goprojects/bin"
				      )
			  )
	)
)

